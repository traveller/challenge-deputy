package au.com.deputy.challenge.networking


data class HttpRequest(
        val method: String,
        val url: String,
        val headers: HttpHeaders = HttpHeaders(),
        val body: String? = null
)
package au.com.deputy.challenge.networking


interface HttpClient {

    suspend fun executeRequest(request: HttpRequest): HttpResponse
}
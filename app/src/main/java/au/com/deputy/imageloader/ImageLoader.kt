package au.com.deputy.imageloader

import android.widget.ImageView

interface ImageLoader {

    fun load(targetView: ImageView, href: String?)
}
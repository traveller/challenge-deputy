package au.com.deputy.challenge.startendshift

import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import au.com.deputy.challenge.R
import au.com.deputy.challenge.launch.MainActivityViewModel
import au.com.deputy.challenge.location.LocationListener
import au.com.deputy.challenge.location.LocationProvider
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.start_end_shift_fragment.*
import javax.inject.Inject


class StartEndShiftFragment : Fragment() {

    @Inject
    lateinit var factory: ViewModelProvider.Factory
    @Inject
    lateinit var locationProvider: LocationProvider


    companion object {
        fun newInstance() = StartEndShiftFragment()
    }

    private val startEndViewModel: StartEndViewModel by lazy(LazyThreadSafetyMode.NONE) {
        ViewModelProviders.of(this, factory).get(StartEndViewModel::class.java)
    }
    private val mainActivityViewModel: MainActivityViewModel by lazy(LazyThreadSafetyMode.NONE) {
        ViewModelProviders.of(activity!!, factory).get(MainActivityViewModel::class.java)
    }

    /* ********* */
    /* LIFECYCLE */
    /* ********* */
    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.start_end_shift_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initLiveData()
        shift_action_button.setOnClickListener { onShiftButtonClick() }
        shift_history_button.setOnClickListener { onShiftHistoryButtonClick() }
    }

    override fun onStart() {
        super.onStart()
        getLocation()
    }

    override fun onStop() {
        locationProvider.stopListeningForUpdates(context)
        super.onStop()
    }

    private val locationUpdateHandler = object : LocationListener {
        override fun permissionNotGranted(missingPermission: Set<String>) {
            requestPermissions(missingPermission)
        }

        override fun servicesNotEnabled() {
            if (context != null) {
                Toast.makeText(context, R.string.location_services_not_enabled, Snackbar.LENGTH_LONG).show()
            }
        }

        override fun newLocation(lastLocation: Location) {
            startEndViewModel.setLastLocation(lastLocation.latitude, lastLocation.longitude)
        }
    }

    /* ************ */
    /* INITIALISERS */
    /* ************ */
    private fun initLiveData() {
        startEndViewModel.data.observe(this, Observer {
            handleStopEndShiftUpdate(it)
        })
        startEndViewModel.init()
    }

    /* ******* */
    /* ACTIONS */
    /* ******* */
    private fun onShiftButtonClick() {
        startEndViewModel.shiftAction()
    }

    private fun onShiftHistoryButtonClick() {
        mainActivityViewModel.showShiftHistory()
    }

    private fun getLocation() {
        locationProvider.startListeningForUpdates(context, locationUpdateHandler)
    }

    /* ********* */
    /* REACTIONS */
    /* ********* */
    private fun handleStopEndShiftUpdate(startEndModel: StartEndModel) {
        if (startEndModel.isLoading) {
            shift_action_button.text = getString(R.string.loading_shifts)
        } else {
            when (startEndModel.hasOpenShift) {
                true -> shift_action_button.text = getString(R.string.end_shift)
                else -> shift_action_button.text = getString(R.string.start_shift)
            }
        }
    }

    private fun requestPermissions(missingPermissions: Set<String>) {
        if (activity != null && missingPermissions.isNotEmpty()) {
            ActivityCompat.requestPermissions(activity!!, missingPermissions.toTypedArray(), 1337)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            1337 -> {
                // IF REQUEST IS CANCELLED, THE RESULT ARRAYS ARE EMPTY
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    // PERMISSION WAS GRANTED
                    getLocation()
                } else {
                    // PERMISSION DENIED
                    locationProvider.stopListeningForUpdates(activity!!)
                }
                return
            }
        }
    }
}

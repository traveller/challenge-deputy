package au.com.deputy.challenge.di.module

import au.com.deputy.challenge.di.ActivityScope
import au.com.deputy.challenge.di.FragmentScope
import au.com.deputy.challenge.launch.MainActivity
import au.com.deputy.challenge.shifthistory.ShiftHistoryFragment
import au.com.deputy.challenge.startendshift.StartEndShiftFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class AndroidBindingModule {

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun mainActivity(): MainActivity

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun startEndFragment(): StartEndShiftFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun shiftHistoryFragment(): ShiftHistoryFragment
}
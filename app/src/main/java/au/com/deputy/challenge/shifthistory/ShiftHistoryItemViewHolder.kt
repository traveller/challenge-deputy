package au.com.deputy.challenge.shifthistory

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import au.com.deputy.challenge.R
import au.com.deputy.challenge.domain.ShiftDetails
import au.com.deputy.imageloader.ImageLoader
import au.com.deputy.imageloader.ImageLoaderImpl
import org.jetbrains.anko.find


class ShiftHistoryItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    companion object {
        const val layoutId = R.layout.list_item_shift_history
    }

    private val imageLoader: ImageLoader = ImageLoaderImpl

    private val image: ImageView = itemView.find(R.id.shift_image)
    private val startTime: TextView = itemView.find(R.id.shift_start_time)
    private val startLatitude: TextView = itemView.find(R.id.shift_start_latitude)
    private val startLongitude: TextView = itemView.find(R.id.shift_start_longitude)
    private val endTime: TextView = itemView.find(R.id.shift_end_time)
    private val endLatitude: TextView = itemView.find(R.id.shift_end_latitude)
    private val endLongitude: TextView = itemView.find(R.id.shift_end_longitude)

    fun bindView(shiftDetails: ShiftDetails) {
        imageLoader.load(image, shiftDetails.image)

        startTime.text = "Start: %s".format(shiftDetails.start.toString())
        startLatitude.text = "Lat: %s".format(shiftDetails.startLatitude)
        startLongitude.text = "Lon: %s".format(shiftDetails.startLongitude)

        endTime.text = "End: %s".format(shiftDetails.end.toString())
        endLatitude.text = "Lat: %s".format(shiftDetails.endLatitude)
        endLongitude.text = "Lon: %s".format(shiftDetails.endLongitude)

    }
}
package au.com.deputy.challenge.remote

import au.com.deputy.challenge.domain.ShiftAction
import au.com.deputy.challenge.domain.ShiftDetails

interface RemoteApi {

    suspend fun getAllShifts(): List<ShiftDetails>

    suspend fun startShift(shiftAction: ShiftAction): String

    suspend fun endShift(shiftAction: ShiftAction): String
}

package au.com.deputy.challenge.remote

import au.com.deputy.challenge.domain.ShiftAction
import au.com.deputy.challenge.domain.ShiftDetails
import au.com.deputy.challenge.networking.HttpClient
import au.com.deputy.challenge.networking.HttpHeaders
import au.com.deputy.challenge.networking.HttpRequest
import au.com.deputy.challenge.networking.HttpResponse
import au.com.deputy.challenge.remote.model.ShiftStartEnd
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.InputStreamReader
import java.util.*
import javax.inject.Inject


class RemoteApiImpl
@Inject constructor(private val client: HttpClient)
    : RemoteApi {

    companion object {
        private const val BASE_URL = "https://apjoqdqpi3.execute-api.us-west-2.amazonaws.com/dmc"
        private const val SHA1_AUTH = "710ba60fc81f84da552f6d1365976d6e0e731a30" //SHA-1("sini")
    }

    private val logger: Logger = LoggerFactory.getLogger(RemoteApiImpl::class.java)

    private val gson: Gson = GsonBuilder().create()

    override suspend fun getAllShifts(): List<ShiftDetails> = withContext(Dispatchers.IO) {
        val endpoint = "/shifts"
        val request = HttpRequest(
                "GET",
                "%s%s".format(BASE_URL, endpoint),
                HttpHeaders().addHeader("Authorization", "Deputy %s".format(SHA1_AUTH))
        )
        val response = client.executeRequest(request)
        unmarshalGetAllShiftsResponse(response)
    }

    override suspend fun startShift(shiftAction: ShiftAction): String = withContext(Dispatchers.IO) {
        val endpoint = "/shift/start"
        executeShiftAction(shiftAction, endpoint)
    }

    override suspend fun endShift(shiftAction: ShiftAction): String = withContext(Dispatchers.IO) {
        val endpoint = "/shift/end"
        executeShiftAction(shiftAction, endpoint)
    }

    private suspend fun executeShiftAction(shiftAction: ShiftAction, endpoint: String): String {
        val requestData = ShiftStartEnd(shiftAction.latitude, shiftAction.longitude)
        val request = HttpRequest(
                "POST",
                "%s%s".format(BASE_URL, endpoint),
                HttpHeaders().addHeader("Authorization", "Deputy %s".format(SHA1_AUTH)),
                gson.toJson(requestData)
        )
        val response = client.executeRequest(request)
        return unmarshalPostShiftActionResponse(response)
    }

    private fun unmarshalGetAllShiftsResponse(response: HttpResponse): List<ShiftDetails> {
        logger.info("unmarshalGetAllShiftsResponse")
        val result = if (response.hasBody) {
            val listType = object : TypeToken<ArrayList<ShiftDetails>>() {}.type
            val reader = InputStreamReader(response.body, Charsets.UTF_8)
            val shifts: List<ShiftDetails> = gson.fromJson(reader, listType)
            logger.debug("unmarshalGetAllShiftsResponse | has body")
            shifts
        } else {
            logger.debug("unmarshalGetAllShiftsResponse | empty (or 'null') body")
            emptyList()
        }

        logger.debug("unmarshalGetAllShiftsResponse | return %s Shift(s)".format(result.size))

        return result
    }

    private fun unmarshalPostShiftActionResponse(response: HttpResponse): String {
        logger.info("unmarshalPostShiftActionResponse")
        val result = if (response.hasBody) {
            val reader = InputStreamReader(response.body, Charsets.UTF_8)
            val result: String = gson.fromJson(reader, String::class.java)
            logger.debug("unmarshalPostShiftActionResponse| has body")
            result
        } else {
            logger.debug("unmarshalPostShiftActionResponse | empty (or 'null') body")
            ""
        }

        logger.debug("unmarshalGetAllShiftsResponse | shift action response: $result")
        return result
    }
}
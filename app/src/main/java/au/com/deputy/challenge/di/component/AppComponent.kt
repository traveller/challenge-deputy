package au.com.deputy.challenge.di.component

import au.com.deputy.challenge.App
import au.com.deputy.challenge.di.module.AndroidBindingModule
import au.com.deputy.challenge.di.module.CommonAppModule
import au.com.deputy.challenge.di.module.ViewModelModule
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton


@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    AndroidBindingModule::class,
    CommonAppModule::class,
    ViewModelModule::class
])
interface AppComponent : AndroidInjector<App> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<App>()
}
package au.com.deputy.challenge.launch

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import javax.inject.Inject


data class MainActivityModel(val showList: Boolean)

class MainActivityViewModel @Inject constructor(): ViewModel() {

    val mainLiveData: MutableLiveData<MainActivityModel> = MutableLiveData()

    fun showShiftHistory() {
        mainLiveData.postValue(MainActivityModel(true))
    }
}
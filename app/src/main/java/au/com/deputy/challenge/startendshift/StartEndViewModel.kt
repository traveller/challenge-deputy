package au.com.deputy.challenge.startendshift

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import au.com.deputy.challenge.domain.ShiftAction
import au.com.deputy.challenge.repository.ShiftRepo
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext


class StartEndViewModel @Inject constructor(
        private val repo: ShiftRepo
) : ViewModel(), CoroutineScope {

    private val job = Job()

    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    private var lastLatitude: String = "0.0000"
    private var lastLongitude: String = "0.0000"
    val data: MutableLiveData<StartEndModel> = MutableLiveData()

    fun shiftAction() {
        val shift = repo.getMyLastShift()
        val action = ShiftAction(lastLatitude, lastLongitude)

        this.launch(context = coroutineContext) {
            if (shift?.isOpen() == true) {
                repo.endShift(action)
            } else {
                repo.startShift(action)
            }
            refresh()
        }
    }

    fun init() {
        refresh()
    }

    private fun refresh() {
        this.launch(context = coroutineContext) {
            data.postValue(StartEndModel(false, true))
            repo.getMyShifts()

            val shift = repo.getMyLastShift()
            if (shift != null) {
                val hasOpenShift = shift.isOpen()
                data.postValue(StartEndModel(hasOpenShift))
            }
        }
    }

    fun setLastLocation(latitude: Double, longitude: Double) {
        lastLatitude = "%.4f".format(latitude)
        lastLongitude = "%.4f".format(longitude)
    }
}

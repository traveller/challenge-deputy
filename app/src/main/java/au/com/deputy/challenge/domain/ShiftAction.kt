package au.com.deputy.challenge.domain

data class ShiftAction(val latitude: String, val longitude: String)

data class ShiftDetails(
        val id: Int,
        val start: String,
        val end: String,
        val startLatitude: String,
        val startLongitude: String,
        val endLatitude: String,
        val endLongitude: String,
        val image: String
) {
    fun isOpen(): Boolean {
        return end.isBlank()
    }
}
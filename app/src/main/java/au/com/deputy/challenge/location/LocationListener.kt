package au.com.deputy.challenge.location

import android.location.Location


interface LocationListener {

    fun permissionNotGranted(missingPermission: Set<String>)

    fun servicesNotEnabled()

    fun newLocation(lastLocation: Location)
}
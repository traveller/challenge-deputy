package au.com.deputy.challenge.shifthistory

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import au.com.deputy.challenge.R
import au.com.deputy.challenge.domain.ShiftDetails
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.shift_history_fragment.*
import timber.log.Timber
import javax.inject.Inject


class ShiftHistoryFragment : Fragment() {

    @Inject
    lateinit var factory: ViewModelProvider.Factory

    companion object {
        fun newInstance() = ShiftHistoryFragment()
    }

    private val shiftsViewModel by lazy(LazyThreadSafetyMode.NONE) {
        ViewModelProviders.of(this, factory).get(ShiftHistoryViewModel::class.java)
    }

    private lateinit var recyclerView: RecyclerView
    private lateinit var recyclerAdapter: ShiftHistoryAdapter

    /* ********* */
    /* LIFECYCLE */
    /* ********* */
    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.shift_history_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        initLiveData()
    }

    /* ************ */
    /* INITIALISERS */
    /* ************ */
    private fun initRecyclerView() {
        recyclerView = recycler_view
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = GridLayoutManager(context, 1)

        val decoration = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
        recyclerView.addItemDecoration(decoration)

        recyclerAdapter = ShiftHistoryAdapter()
        recyclerView.adapter = recyclerAdapter
    }

    private fun initLiveData() {
        shiftsViewModel.shiftsLiveData.observe(this, Observer {
            Timber.d("initLiveData | shiftsLiveData | observed | count=${it.size}")
            updateList(it)
        })
        shiftsViewModel.init()
    }

    /* ********* */
    /* REACTIONS */
    /* ********* */
    private fun updateList(items: List<ShiftDetails>) {
        recyclerAdapter.setItems(items)
        recyclerAdapter.notifyDataSetChanged()
    }
}

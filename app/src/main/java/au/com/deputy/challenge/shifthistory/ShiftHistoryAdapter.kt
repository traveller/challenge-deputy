package au.com.deputy.challenge.shifthistory

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import au.com.deputy.challenge.domain.ShiftDetails
import timber.log.Timber


class ShiftHistoryAdapter : RecyclerView.Adapter<ShiftHistoryItemViewHolder>() {

    private val items: MutableList<ShiftDetails> = mutableListOf()

    // BUILD ViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShiftHistoryItemViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(ShiftHistoryItemViewHolder.layoutId, parent, false)
        return ShiftHistoryItemViewHolder(v)
    }

    // BIND VALUES TO THE ViewHolder
    override fun onBindViewHolder(holder: ShiftHistoryItemViewHolder, position: Int) {
        holder.bindView(getItem(position))
    }

    override fun getItemId(position: Int): Long {
        return items[position].id.toLong()
    }

    override fun getItemCount(): Int {
        return items.size
    }

    private fun getItem(position: Int): ShiftDetails {
        return items[position]
    }

    fun setItems(shifts: List<ShiftDetails>) {
        Timber.i("setItems")
        Timber.d("setItems | shifts count=${shifts.size}")
        items.clear()
        items.addAll(shifts)
    }
}
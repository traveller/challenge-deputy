package au.com.deputy.challenge.location

import android.content.Context

interface LocationProvider {

    fun startListeningForUpdates(context: Context?, locationCallback: LocationListener)

    fun stopListeningForUpdates(context: Context?)
}

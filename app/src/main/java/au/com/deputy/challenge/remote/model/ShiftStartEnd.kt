package au.com.deputy.challenge.remote.model

import java.text.SimpleDateFormat
import java.util.*

data class ShiftStartEnd(
        val latitude: String,
        val longitude: String,
        val time: String = DateIS8601Formatter.fromDate(Date()))


object DateIS8601Formatter {
    fun fromDate(date: Date): String {
        val df = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH)
        df.timeZone = TimeZone.getTimeZone("GMT")
        return df.format(date)
    }
}
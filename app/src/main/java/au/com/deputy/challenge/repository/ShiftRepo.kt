package au.com.deputy.challenge.repository

import au.com.deputy.challenge.domain.ShiftAction
import au.com.deputy.challenge.domain.ShiftDetails


interface ShiftRepo {

    suspend fun getMyShifts(skipCache: Boolean = false): List<ShiftDetails>

    fun getMyLastShift(): ShiftDetails?

    suspend fun startShift(action: ShiftAction): String

    suspend fun endShift(action: ShiftAction): String
}
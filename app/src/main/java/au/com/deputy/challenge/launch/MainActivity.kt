package au.com.deputy.challenge.launch

import android.os.Bundle
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import au.com.deputy.challenge.R
import au.com.deputy.challenge.shifthistory.ShiftHistoryFragment
import au.com.deputy.challenge.startendshift.StartEndShiftFragment
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.find
import javax.inject.Inject


class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var factory: ViewModelProvider.Factory

    private val contentHolder: FrameLayout by lazy { find<FrameLayout>(R.id.content_holder) }

    private val mainActivityViewModel by lazy(LazyThreadSafetyMode.NONE) {
        ViewModelProviders.of(this, factory).get(MainActivityViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setToolbar()
        initLiveData()

        setContent(StartEndShiftFragment.newInstance())
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 1) {
            super.onBackPressed()
        }
    }

    /* ************ */
    /* INITIALISERS */
    /* ************ */
    private fun initLiveData() {
        mainActivityViewModel.mainLiveData.observe(this, Observer {
            handleNavigationUpdate(it)
        })
    }

    /* ********* */
    /* REACTIONS */
    /* ********* */
    private fun setContent(fragment: Fragment) {
        supportFragmentManager
                .beginTransaction()
                .replace(contentHolder.id, fragment)
                .addToBackStack(null)
                .commit()
    }

    private fun setToolbar(title: String? = null) {
        toolbar.title = title ?: getString(R.string.app_name)
    }

    private fun handleNavigationUpdate(model: MainActivityModel) {
        if (model.showList) {
            setContent(ShiftHistoryFragment.newInstance())
        }
    }
}
package au.com.deputy.challenge

import au.com.deputy.challenge.di.component.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import timber.log.Timber


class App : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<App> {
        return DaggerAppComponent.builder().create(this)
    }

    override fun onCreate() {
        super.onCreate()

        applicationInjector().inject(this)

        initLogging()
    }

    private fun initLogging() {
        Timber.plant(Timber.DebugTree())
    }
}
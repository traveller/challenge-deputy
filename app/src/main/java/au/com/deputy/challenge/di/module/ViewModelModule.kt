package au.com.deputy.challenge.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import au.com.deputy.challenge.launch.MainActivityViewModel
import au.com.deputy.challenge.shifthistory.ShiftHistoryViewModel
import au.com.deputy.challenge.startendshift.StartEndViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap


@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MainActivityViewModel::class)
    internal abstract fun mainActivityViewModel(viewModel: MainActivityViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(StartEndViewModel::class)
    internal abstract fun startStopViewModel(viewModel: StartEndViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ShiftHistoryViewModel::class)
    internal abstract fun shiftHistoryViewModel(viewModel: ShiftHistoryViewModel): ViewModel

}
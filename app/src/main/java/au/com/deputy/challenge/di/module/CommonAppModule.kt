package au.com.deputy.challenge.di.module

import au.com.deputy.challenge.location.LocationProvider
import au.com.deputy.challenge.location.LocationProviderImpl
import au.com.deputy.challenge.networking.HttpClient
import au.com.deputy.challenge.networking.HttpClientImpl
import au.com.deputy.challenge.remote.RemoteApi
import au.com.deputy.challenge.remote.RemoteApiImpl
import au.com.deputy.challenge.repository.RemoteShiftRepoImpl
import au.com.deputy.challenge.repository.ShiftRepo
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class CommonAppModule {

    @Provides
    fun locationProvider(): LocationProvider = LocationProviderImpl()

    @Provides
    @Singleton
    fun networkClient(): HttpClient = HttpClientImpl()

    @Provides
    @Singleton
    fun remoteApi(httpClient: HttpClient): RemoteApi = RemoteApiImpl(httpClient)

    @Provides
    @Singleton
    fun shiftRepo(remoteApi: RemoteApi): ShiftRepo = RemoteShiftRepoImpl(remoteApi)
}
package au.com.deputy.challenge.shifthistory

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import au.com.deputy.challenge.domain.ShiftDetails
import au.com.deputy.challenge.repository.ShiftRepo
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class ShiftHistoryViewModel
@Inject constructor(private val repo: ShiftRepo)
    : ViewModel(), CoroutineScope {

    private val job = Job()

    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    fun init() {
        refresh()
    }

    val shiftsLiveData: MutableLiveData<List<ShiftDetails>> = MutableLiveData()

    private fun refresh() {
        this.launch(context = coroutineContext) {
            shiftsLiveData.postValue(repo.getMyShifts())
        }
    }
}

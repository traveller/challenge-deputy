package au.com.deputy.challenge.startendshift

data class StartEndModel(val hasOpenShift: Boolean, val isLoading: Boolean = false)

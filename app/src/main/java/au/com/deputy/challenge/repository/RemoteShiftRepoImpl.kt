package au.com.deputy.challenge.repository

import au.com.deputy.challenge.domain.ShiftAction
import au.com.deputy.challenge.domain.ShiftDetails
import au.com.deputy.challenge.remote.RemoteApi
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.withContext
import java.util.concurrent.atomic.AtomicBoolean
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext


class RemoteShiftRepoImpl
@Inject constructor(private val remoteApi: RemoteApi)
    : ShiftRepo, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    private val shifts = mutableListOf<ShiftDetails>()
    private var isFirstLoad: AtomicBoolean = AtomicBoolean(true)

    override suspend fun getMyShifts(skipCache: Boolean): List<ShiftDetails> = withContext(Dispatchers.IO) {

        if (isFirstLoad.get() || skipCache) {
            shifts.clear()
            shifts.addAll(remoteApi.getAllShifts())
            isFirstLoad.getAndSet(false)
        }

        shifts
    }

    override fun getMyLastShift(): ShiftDetails? {
        return if (shifts.isNotEmpty()) {
            shifts[shifts.lastIndex]
        } else {
            null
        }
    }

    override suspend fun startShift(action: ShiftAction): String = withContext(Dispatchers.IO) {
        val result = remoteApi.startShift(action)
        getMyShifts(true)

        result
    }

    override suspend fun endShift(action: ShiftAction): String = withContext(Dispatchers.IO) {
        val result = remoteApi.endShift(action)
        getMyShifts(true)

        result
    }
}
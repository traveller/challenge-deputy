package au.com.deputy.challenge.location

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Looper
import androidx.core.content.ContextCompat
import com.google.android.gms.location.*
import timber.log.Timber


class LocationProviderImpl : LocationProvider {

    private val locationRequest = LocationRequest()
    private var locationSettingsRequest: LocationSettingsRequest
    private var locationCallback: LocationListener? = null

    private val locationResultCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            locationCallback?.newLocation(locationResult.lastLocation)
        }
    }

    init {
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 10 * 1000 // 10 seconds
        locationRequest.fastestInterval = 2 * 1000 // 2 seconds

        locationSettingsRequest = LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest)
                .build()
    }


    override fun startListeningForUpdates(context: Context?, locationCallback: LocationListener) {
        Timber.i("startListeningForUpdates")
        if (context != null) {
            val missingPermissions = mutableSetOf<String>()

            // Check whether location settings are satisfied
            val settingsClient = LocationServices.getSettingsClient(context)
            val r = settingsClient.checkLocationSettings(locationSettingsRequest)
            r.addOnFailureListener {
                Timber.i("startListeningForUpdates | settings failure")
                locationCallback.servicesNotEnabled()
            }

            if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(Manifest.permission.ACCESS_COARSE_LOCATION)
            }
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(Manifest.permission.ACCESS_FINE_LOCATION)
            }

            if (missingPermissions.isEmpty()) {
                Timber.i("startListeningForUpdates | permissions granted")
                this.locationCallback = locationCallback
                val fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)
                fusedLocationClient.requestLocationUpdates(
                        locationRequest,
                        locationResultCallback,
                        Looper.myLooper()
                )
            } else {
                Timber.i("startListeningForUpdates | missing permissions")
                locationCallback.permissionNotGranted(missingPermissions)
            }
        }
    }

    override fun stopListeningForUpdates(context: Context?) {
        Timber.i("stopListeningForUpdates")
        if (context != null) {
            val fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)
            fusedLocationClient?.removeLocationUpdates(locationResultCallback)
            this.locationCallback = null
        }
    }
}